%! TeX program = lualatex
\documentclass[
        egregdoesnotlikesansseriftitles,
        paper=a4,
        fontsize=12pt,
        DIV=calc
]{scrarticle}

\usepackage[no-math]{fontspec}
\usepackage{polyglossia}
\setmainlanguage[variant=british]{english}

\usepackage{geometry}
 \geometry{
         total={170mm,257mm},
         left=20mm,
         top=20mm,
         bottom=30mm,
 }

\usepackage{siunitx}
\usepackage{etoolbox}
\usepackage[justification=centering]{caption}
\usepackage{subcaption}
\usepackage{multirow, tabularx}

\usepackage{pgfplots}
\usepgfplotslibrary{groupplots,dateplot}
\usetikzlibrary{patterns,shapes.arrows}
\pgfplotsset{compat=newest}
\pgfplotsset{every tick label/.append style={font=\scriptsize}}
\pgfplotsset{every axis label/.append style={font=\scriptsize}}

% Macro to redefine the style of subfigures
\newcommand{\parens}[1]{%
(#1)}
\let\oldsubfigure\thesubfigure
\renewcommand\thesubfigure{\parens{\oldsubfigure}}

\setkomafont{disposition}{\mdseries\rmfamily}
% Different tilte
\makeatletter
\renewcommand*{\@maketitle}{%
        \global\@topnum=\z@
        \setparsizes{\z@}{\z@}{\z@\@plus 1fil}\par@updaterelative
        \ifx\@titlehead\@empty \else
                \begin{minipage}[t]{\textwidth}
                        \usekomafont{titlehead}{\@titlehead\par}%
                \end{minipage}\par
        \fi
        \null
        %\vskip -1em%
        \begin{center}%
                \ifx\@subject\@empty \else
                        {\usekomafont{subject}{\@subject \par}}%
                        \vskip 1em
                \fi
                {\usekomafont{title}{\Huge \@title \par}}%
                \vskip .5em
                {\ifx\@subtitle\@empty\else\usekomafont{subtitle}\@subtitle\par\fi}%
                \vskip 1em
                {%
                        \usekomafont{author}{%
                                \lineskip .5em%
                                \begin{tabular}[t]{c}
                                        \@author
                                \end{tabular}\par
                        \usekomafont{publishers}{%
                                \lineskip .5em%
                                \@publishers\par
                        }%
                }%
        }
        \end{center}%
        \par
        \deffootnote[12pt]{0pt}{0em}{%
                \makebox[9pt][l]{\thefootnotemark}%
        }
}%
\makeatother

% Fonts setup
\defaultfontfeatures[\rmfamily,\sffamily]{Ligatures=TeX}
\setmainfont{TeXGyreTermesX}[
        UprightFont = *-Regular,
        BoldFont = *-Bold,
        ItalicFont = *-Italic,
        BoldItalicFont = *-BoldItalic,
        Extension = .otf]
\usepackage{amsmath}
\setsansfont{LibertinusSans}[
        UprightFont = *-Regular,
        BoldFont = *-Bold,
        ItalicFont = *-Italic,
        Extension = .otf]
\setmonofont{inconsolata}
\usepackage[
        subscriptcorrection,
        nofontinfo,
        zswash,
        mtphrd,
        mtpfrak,
        mtpcal
]{mtpro2}

\usepackage[backend=biber, style=phys, biblabel=brackets]{biblatex}
\addbibresource{./references.bib}

\usepackage[hypertexnames=false, hidelinks]{hyperref}

% Document information
\author{
  Juan David Salcedo Hernández\texorpdfstring{\\ {\large\href{mailto:jdavid.salcedo@udea.edu.co}{jdavid.salcedo@udea.edu.co}}}{}
}

\title{Brief power spectrum analysis of electrocardiogram signals}

\begin{document}
\pgfkeys{/pgf/number format/.cd,1000 sep={\,}}
\maketitle

\section{Introduction}

Striving for methods to accurately detect certain cardiovascular abnormalities
through electrocardiography (ECG), signal processing devices pertaining to the
Fourier transform have been repeatedly put forward under the premise that they
should enable healthcare professionals to recognise significant frequency
features in ECG readings~\cite{ahmed2016ecg}. Recognising the characteristics of
the power spectrum of a given signal as a function of frequency should in turn
streamline heart-related diagnoses because, in doing so, a description of the
electrical activity of the heart can be given in terms of frequency
peaks~\cite{prasad2017detection}. By way of explanation, an electrocardiogram
surveys voltage changes on the heart, which are ultimately related to the
electrical impulses of the nerves that account for the heart's periodic
contraction and relaxation~\cite{ahmed2016ecg}.

In furtherance to the study of an ECG signal, signal filtering must be
emphasised, for this sort of readings can be easily affected by factors
unrelated to the heart's electrical activity itself; typical noise sources in
electrocardiography include possible variation of electrode impedances,
electrode contact noise, motion artefacts, and supply-line interferences within
the signal~\cite{prasad2017detection}. In this succinct report, two real ECG
signals shall be displayed in the power spectrum, thus enabling the discussion
of their frequency components. 
% certain
% noise features whose origin may well be hypothesised.

\section{The discrete Fourier Transform}

The idea of Fourier transforms is pivotal to what shall be presented in this
report; therefore, a basic description thereof is worth mentioning. The Fourier
transform of a time-function $f : A \subset \mathbb{R} \to \mathbb{R}$ is
defined as a new frequency-function $\mathcal{F}(f) : \mathbb{R} \to \mathbb{C}$,
\begin{equation}\label{eq:1}
\mathcal{F}(f)(\nu) = \int_{-\infty}^{\infty} f(t) e^{-2\pi i \nu t} dt, \quad
\text{for all $\nu \in \mathbb{R}$};
\end{equation}
the quantity $|\mathcal{F}(f)|$ is called the \emph{power spectrum} of $f$.

This seems quite reasonable when viewed in analytical terms; nevertheless,
most applications of the Fourier transform are restricted to the computational
domain, which is to say that discrete signals and a discrete version
of~\autoref{eq:1} must be used instead. The discrete Fourier transform
algorithm is provided with a list of values $(a_1,\ldots,a_n)$, and assumes that
they are the image, under some function of time, of an evenly-spaced time list
$(t_1,\ldots,t_n)$, i.e., a list with $t_j - t_{j-1} = t_n/n$. It further
assumes that $t_0 = 0$, and $t_n = 1$, so that $t_j - t_{j-1} = 1/n$;
consequently, the time pertaining to $a_j$ becomes $j/n$. Moreover, if $\Delta t
= t_j - t_{j-1} = 1/n$, we can take a frequency of, at most, $n$. These
considerations enable the formal construction of the
discrete Fourier transform,
\begin{equation}        
A_\nu =  \sum_{j=0}^{n-1} a_j e^{-2\pi i \nu \frac{j}{n}}, \quad 0 \le \nu < n.
\end{equation}
Notice that the $\Delta t$ factor is missing in the equation above, this is
because the real time resolution is unknown by the algorithm, so it should be
included by the user after the computation has been completed. This tool will be
used to extract the power spectrum as $|A_\nu \Delta t|$.

\section{Considerations on time resolution and sampling frequency}

Whenever a discrete transform procedure is performed on a signal, one needs to
be concerned with the problem of finding the adequate sampling frequency to
potentially reconstruct the original function from its power spectrum.
Fortunately, the Nyquist-Shannon Theorem stipulates a condition to achieve this
adequate specification of the frequency domain, namely, if one wishes to
describe the signal up to a frequency $\nu_{\text{max}}$, then the time
resolution one should use is bounded as
\begin{equation}\label{eq:3}
        \Delta t \le \frac{1}{2\nu_{\text{max}}}.
\end{equation}
Consequently, for a fixed time resolution, the frequency is at most
$1/(2\Delta t)$. 

On the other hand, if the sampling frequency is constant, then it must be
$1/\Delta t$. This together with~\autoref{eq:3} means that the statement
of~\autoref{eq:3} can be reworded as follows:
\begin{quote}
        If you want to faithfully represent the spectrum of a signal up to
        frequency $\nu$, then you should sample at a rate of, at least, $2\nu$.
\end{quote}

\section{Simulated signals and their power spectra}
\begin{figure*}[h]
        \centering
        {\phantomsubcaption\label{fig:1a}}
        {\phantomsubcaption\label{fig:1b}}
        \tikz\node[
        inner sep=2pt,
        label={[anchor=south east]north west:\subref{fig:1a}}
        ] {\input{./pgfplots/simulated_signal_1.tex}};
        \tikz\node[
        inner sep=2pt,
        label={[anchor=south east]north west:\subref{fig:1b}}
        ] {\input{./pgfplots/simulated_signal_2.tex}};
        \caption{
                Raw simulated electrocardiogram signals. \subref{fig:1a}~Signal
                1; \subref{fig:1b}~Signal 2.
        }\label{fig:1}
\end{figure*}

To begin with, we present two simulated ECG signals, for which we compute the
discrete Fourier transform; \autoref{fig:1} shows these signals (in volts) as
functions of time. Granted that the total sampling time was \SI{10}{s}, the time
resolution for the discrete Fourier transform could be determined as
$(\text{sampling time})/(\text{length of the signal array)}$. Once this was
done, both the power spectra and the frequency domains were computed as required
by the Nyquist-Shannon Theorem. The results are presented
in \autoref{fig:2}.

The simulated signals should be studied by dismissing all high power points at low
frequencies, which are not of interest. The power spectrum
of signal 1,~\autoref{fig:2a}, attains its first maximum at \SI{1.80}{Hz},
whereas that of signal 2,~\autoref{fig:2a}, does so at \SI{1.10}{Hz}. These
values presumably correspond to the fundamental frequencies at which the
voltages on the electrodes are changing, although they are not the ones with the most
contribution to the signal as a whole, because they are not the highest spectral values.

The aforementioned frequencies can be used to speculate whether these readings
would be typical for a healthy patient: According to~\cite{chang201835}, the
human heart beats at a rate ranging from \SIrange{0.6}{2.0}{Hz}, or thereabouts;
therefore, even if there may be factors that are not accounted for in this
spectrum, we may hypothesise that the heart rate which these electrocardiograms
describe lie within the normal range.
\begin{figure*}[htpb]
        \centering
        {\phantomsubcaption\label{fig:2a}}
        {\phantomsubcaption\label{fig:2b}}
        \tikz\node[
        inner sep=2pt,
        label={[anchor=south east]north west:\subref{fig:2a}}
        ] {\input{./pgfplots/PSD_simulated_signal_1.tex}};
        \tikz\node[
        inner sep=2pt,
        label={[anchor=south east]north west:\subref{fig:2b}}
        ] {\input{./pgfplots/PSD_simulated_signal_2.tex}};
        \caption{
                Power spectrum as a function of frequency for simulated
                signals. These charts have been scaled to display frequency
                values between \SI{0}{Hz} and \SI{10}{Hz}.
                \subref{fig:2a}~Signal 1; \subref{fig:2b}~Signal 2.
        }\label{fig:2}
\end{figure*}

\section{Real signals and their power spectra}

To conclude, we present two EGC signals that were actually taken on patients. As
before, the discrete Fourier transform is computed and analysed;
\autoref{fig:3}~shows these signals (in volts) as functions of time.
\begin{figure*}[t!]
        \centering
        {\phantomsubcaption\label{fig:3a}}
        {\phantomsubcaption\label{fig:3b}}
        \tikz\node[
        inner sep=2pt,
        label={[anchor=south east]north west:\subref{fig:3a}}
        ] {\input{./pgfplots/real_signal_1.tex}};
        \tikz\node[
        inner sep=2pt,
        label={[anchor=south east]north west:\subref{fig:3b}}
        ] {\input{./pgfplots/real_signal_2.tex}};
        \caption{
                Raw real electrocardiogram signals. \subref{fig:3a}~Signal
                3; \subref{fig:3b}~Signal 4.
        }\label{fig:3}
\end{figure*}

\begin{figure*}[b!]
        \centering
        {\phantomsubcaption\label{fig:4a}}
        {\phantomsubcaption\label{fig:4b}}
        \tikz\node[
        inner sep=2pt,
        label={[anchor=south east]north west:\subref{fig:4a}}
        ] {\input{./pgfplots/PSD_real_signal_1.tex}};
        \tikz\node[
        inner sep=2pt,
        label={[anchor=south east]north west:\subref{fig:4b}}
        ] {\input{./pgfplots/PSD_real_signal_2.tex}};
        \caption{
                Power spectrum as a function of frequency for real
                signals. These charts have
                been scaled to display frequency values between 0 Hz and 10 Hz.
                \subref{fig:4a}~Signal 3; \subref{fig:4b}~Signal 4.
        }\label{fig:4}
\end{figure*}

In this case, the sampling time is unknown, but the sampling rate is given at
\SI{1024}{Hz}; therefore, both the appropriate time resolution and the frequency
domain can be easily computed in accordance with the Nyquist-Shannon Theorem.
The results of the discrete Fourier transform as applied to these datasets are
presented in \autoref{fig:4}.

It was estimated that the power spectrum of
signal 3, \autoref{fig:4a}, attains its most notable and first maximum at a
frequency of \SI{1.17}{Hz}. Moreover, since there is no presence of high power
frequencies near 0, we can infer that the signal has been previously
de-noised.

The original spectrum of signal 4 has notable high
power frequencies near 0, which indicates a much noisier signal overall. In order
to reduce the uninteresting lower frequency contributions, we take the
original signal in~\autoref{fig:3b} and add it together with the negative of its
average value, then we proceed with the Fourier transform, whose result is seen
in~\autoref{fig:4b}. Thus for the spectrum of signal 4, if lower
frequencies are ignored, there is a maximum power frequency at \SI{1.13}{Hz}.
As before, these heart rates lie within the normal range.

\section*{Addendum: Noise in real signals}

There exists at least one noise contribution in the real signals that can be
straightforwardly explained. Indeed, both charts in~\autoref{fig:5} exhibit high
power at \SI{60}{Hz}, which is actually to be expected because of the mains
electricity interference in the reading.
\begin{figure*}[htpb]
        \centering
        {\phantomsubcaption\label{fig:5a}}
        {\phantomsubcaption\label{fig:5b}}
        \tikz\node[
        inner sep=2pt,
        label={[anchor=south east]north west:\subref{fig:5a}}
        ] {\input{./pgfplots/rawPSD_real_signal_1.tex}};
        \tikz\node[
        inner sep=2pt,
        label={[anchor=south east]north west:\subref{fig:5b}}
        ] {\input{./pgfplots/rawPSD_real_signal_2.tex}};
        \caption{
                Power spectrum as a function of frequency for real
                signals. \subref{fig:5a}~Signal 3; \subref{fig:5b}~Signal 4.
        }\label{fig:5}
\end{figure*}

\printbibliography

\end{document}
