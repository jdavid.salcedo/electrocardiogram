(TeX-add-style-hook
 "main"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("scrarticle" "egregdoesnotlikesansseriftitles" "paper=a4" "fontsize=12pt" "DIV=calc")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("fontspec" "no-math") ("caption" "justification=centering") ("mtpro2" "subscriptcorrection" "nofontinfo" "zswash" "mtphrd" "mtpfrak" "mtpcal") ("hyperref" "hypertexnames=false" "hidelinks")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "./pgfplots/simulated_signal_1"
    "./pgfplots/simulated_signal_2"
    "./pgfplots/PSD_simulated_signal_1"
    "./pgfplots/PSD_simulated_signal_2"
    "./pgfplots/real_signal_1"
    "./pgfplots/real_signal_2"
    "./pgfplots/PSD_real_signal_1"
    "./pgfplots/PSD_real_signal_2"
    "./pgfplots/rawPSD_real_signal_1"
    "./pgfplots/rawPSD_real_signal_2"
    "scrarticle"
    "scrarticle10"
    "fontspec"
    "polyglossia"
    "geometry"
    "siunitx"
    "etoolbox"
    "caption"
    "subcaption"
    "multirow"
    "tabularx"
    "pgfplots"
    "amsmath"
    "mtpro2"
    "hyperref")
   (TeX-add-symbols
    '("parens" 1)
    "oldsubfigure")
   (LaTeX-add-labels
    "eq:3"
    "fig:1a"
    "fig:1b"
    "fig:1"
    "fig:2a"
    "fig:2b"
    "fig:2"
    "fig:3a"
    "fig:3b"
    "fig:3"
    "fig:4a"
    "fig:4b"
    "fig:4"
    "fig:5a"
    "fig:5b")
   (LaTeX-add-polyglossia-langs
    '("english" "mainlanguage" "variant=british")))
 :latex)

