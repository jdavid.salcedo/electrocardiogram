#!/usr/bin/env python3
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import tikzplotlib

# Wrapper functions for discrete Fourier transform
def fourier_transform(func, dt):
    FT = np.fft.fftshift(np.fft.fft(np.fft.fftshift(func)))*dt
    return FT

class Electrocardiogram:
    def __init__(self, file_path, scaling_factor=1, total_time=None, sampling_rate=None):
        # allocate memory space for the single-line document containing the
        # data, then read the single-line document as a list of floats
        signal = open(file_path, 'r')
        self.amplitudes = [float(i)/scaling_factor for i in signal.read().split()]
        self.length = len(self.amplitudes)
        self.mask = np.arange(np.floor(self.length/2), self.length, dtype=int)
        # self.mask = np.arange(np.floor(self.length/2), np.floor(self.length/2)+100, self.dtype=int)

        if total_time == sampling_rate:
            raise ValueError('Specify either total_time or sampling_rate,'\
                             'but not both (or neither)')
        elif isinstance(total_time, (int, float)):
            self.dt = total_time/self.length # as in t_n/n, assuming steady sampling rate
            self.time_domain = np.linspace(0, total_time, num=self.length)
            pass
        elif isinstance(sampling_rate, (int, float)):
            self.dt = 1/sampling_rate
            self.time_domain = np.linspace(0, self.length/sampling_rate,
                                           num=self.length)
            pass

        fourier = fourier_transform(self.amplitudes, self.dt)
        # frequency domain complying with the Nyquist-Shannon sampling theorem
        frequencies = np.arange(-1/(2*self.dt), 1/(2*self.dt), 1/(self.dt*self.length))

        self.power_spectrum = abs(fourier[self.mask])
        self.frequency_domain = frequencies[self.mask]

    def remove_bias(self):
        average_value = np.mean(self.amplitudes)
        self.amplitudes -= average_value
        fourier = fourier_transform(self.amplitudes, self.dt)
        self.power_spectrum = abs(fourier[self.mask])
        return self

    def signal_plot(self, name):
        ''' plots the power spectrum density of the signal
        '''
        fig, ax = plt.subplots()
        ax.plot(self.time_domain, self.amplitudes, 'k-')
        ax.set(ylabel='Amplitude / V', xlabel='Time / s')
        ax.grid(linestyle='dashed')
        tikzplotlib.clean_figure()
        tikzplotlib.save(f'../pgfplots/{name}'\
                         '.tex', axis_height='4cm', axis_width='15cm',
                         textsize=8.0)
                         # extra_axis_parameters={'grid style={dashed,gray}'})

    def PSD_plot(self, name):
        ''' plots the power spectrum density of the signal
        '''
        fig, ax = plt.subplots()
        # scale down the domain
        freq = self.frequency_domain[self.frequency_domain <= 10]
        ax.plot(freq, self.power_spectrum[:len(freq)], 'k-')
        ax.set(ylabel='Power spectrum density', xlabel='Frequency / Hz')
        ax.grid(linestyle='dashed')
        tikzplotlib.clean_figure()
        tikzplotlib.save(f'../pgfplots/{name}'\
                         '.tex', axis_height='4.5cm', axis_width='15cm')

        # get index of peak values
        peak_indices = np.where(self.power_spectrum[:len(freq)] > 0.43)
        # get peak frequencies and remove peaks at 0
        peak_frequencies = freq[peak_indices]
        peak_frequencies = peak_frequencies[0.3 < peak_frequencies]
        print({f'{name}': min(peak_frequencies)})

    def raw_PSD_plot(self, name):
        fig, ax = plt.subplots()
        ax.plot(self.frequency_domain, self.power_spectrum, 'k-')
        ax.set(ylabel='Power spectrum density', xlabel='Frequency / Hz')
        ax.grid(linestyle='dashed')
        tikzplotlib.clean_figure()
        tikzplotlib.save(f'../pgfplots/{name}'\
                         '.tex', axis_height='4.5cm', axis_width='15cm')
