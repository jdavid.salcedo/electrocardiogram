#!/usr/bin/env python3
import signal_processing

if __name__ == '__main__':
    signal_1 = signal_processing.Electrocardiogram('../data/senal_ECG_1.txt',
                                                   total_time=10)
    signal_2 = signal_processing.Electrocardiogram('../data/senal_ECG_2.txt',
                                                   total_time=10)
    signal_3 = signal_processing.Electrocardiogram('../data/senal_real_ECG_1.txt',
                                                   sampling_rate=1024,
                                                   scaling_factor=1000)
    signal_4 = signal_processing.Electrocardiogram('../data/senal_real_ECG_2.txt',
                                                   sampling_rate=1024,
                                                   scaling_factor=1000)
    signal_1.signal_plot('simulated_signal_1')
    signal_2.signal_plot('simulated_signal_2')
    signal_3.signal_plot('real_signal_1')
    signal_4.remove_bias().signal_plot('real_signal_2')
